package praytimes.fakher.com.praytimesandroid;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class MainActivity extends Activity {

    private TextView countryCity;
    private TextView fajer;
    private TextView zuhur;
    private TextView asar;
    private TextView maghrib;
    private TextView icha;
    public static PrayTimeCalcul prayers;

    private GPSTracker gpsTracker;
    private double longitude;
    private double latitude;
    private int prayMethodChoosed;
    private String country = "";
    private String city = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        countryCity = (TextView) findViewById(R.id.country);
        fajer = (TextView) findViewById(R.id.fajr_tv);
        zuhur = (TextView) findViewById(R.id.zuhur_tv);
        asar = (TextView) findViewById(R.id.asser_tv);
        maghrib = (TextView) findViewById(R.id.maghreb_tv);
        icha = (TextView) findViewById(R.id.icha_tv);

        ImageButton settingsButton = (ImageButton) findViewById(R.id.settings_button);

        settingsButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                final CharSequence[] tabMethod = {getString(R.string.Ithna_Ashari),
                        getString(R.string.University_of_Islamic_Sciences_karachi),
                        getString(R.string.Islamic_Society_of_North_America),
                        getString(R.string.Muslim_World_League),
                        getString(R.string.Umm_al_Qura),
                        getString(R.string.Egyptian_General_Authority_of_Survey),
                        getString(R.string.Institute_of_Geophysics_University_of_Tehran),
                        getString(R.string.Custom)};
                final AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                // Set the dialog title
                builder.setInverseBackgroundForced(false);
                builder.setTitle(R.string.pray_time__calcul_method)
                        // Specify the list array, the items to be selected by default (null for none),
                        // and the listener through which to receive callbacks when items are selected
                        .setSingleChoiceItems(tabMethod, 0,
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface arg0, int arg1) {
                                        prayMethodChoosed = arg1;
                                    }
                                })
                                // Set the action buttons
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {

                                //Setting pray calcul method
                                if(tabMethod[prayMethodChoosed].toString().equals(getString(R.string.Ithna_Ashari)))
                                    prayers.setCalcMethod(prayers.getJafari());
                                else if(tabMethod[prayMethodChoosed].toString().equals(getString(R.string.University_of_Islamic_Sciences_karachi)))
                                    prayers.setCalcMethod(prayers.getKarachi());
                                else if(tabMethod[prayMethodChoosed].toString().equals(getString(R.string.Islamic_Society_of_North_America)))
                                    prayers.setCalcMethod(prayers.getISNA());
                                else if(tabMethod[prayMethodChoosed].toString().equals(getString(R.string.Muslim_World_League)))
                                    prayers.setCalcMethod(prayers.getMWL());
                                else if(tabMethod[prayMethodChoosed].toString().equals(getString(R.string.Umm_al_Qura)))
                                    prayers.setCalcMethod(prayers.getMakkah());
                                else if(tabMethod[prayMethodChoosed].toString().equals(getString(R.string.Egyptian_General_Authority_of_Survey)))
                                    prayers.setCalcMethod(prayers.getEgypt());
                                else if(tabMethod[prayMethodChoosed].toString().equals(getString(R.string.Institute_of_Geophysics_University_of_Tehran)))
                                    prayers.setCalcMethod(prayers.getTehran());
                                else
                                    prayers.setCalcMethod(prayers.getCustom());
                            }
                        })
                        .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {


                            }
                        });

                builder.create();
                builder.show();
            }
        });

        ImageButton asserButton = (ImageButton)findViewById(R.id.asser_settings_button);

        asserButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                final CharSequence[] tabAssarMethod = {getString(R.string.standard),
                        getString(R.string.hanafi)};
                final AlertDialog.Builder builderAssar = new AlertDialog.Builder(MainActivity.this);
                // Set the dialog title
                builderAssar.setTitle("asser calcul method")
                        // Specify the list array, the items to be selected by default (null for none),
                        // and the listener through which to receive callbacks when items are selected
                        .setSingleChoiceItems(tabAssarMethod, 0,
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface arg0, int arg1) {
                                        prayMethodChoosed = arg1;
                                    }
                                })
                                // Set the action buttons
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                //setting asar pray method
                                if (tabAssarMethod[prayMethodChoosed].toString().equals(getString(R.string.standard)))
                                    prayers.setAsrJuristic(prayers.getShafii());
                                else
                                    prayers.setAsrJuristic(prayers.getHanafi());
                            }
                        })
                        .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {

                            }
                        });

                builderAssar.create();
                builderAssar.show();
            }
        });

        //getting position
        gpsTracker = new GPSTracker(MainActivity.this);


        if(gpsTracker.canGetLocation()){

            latitude = gpsTracker.getLatitude();
            longitude = gpsTracker.getLongitude();

            country = gpsTracker.getCountryName(this);
            city = gpsTracker.getLocality(this);
            countryCity.setText(country + "/" + city);

        }else{
            // can't get location
            // gpsTracker or Network is not enabled
            // Ask user to enable gpsTracker/network in settings
            gpsTracker.showSettingsAlert();
        }

        //Pray time calcul
        double timezone = ((TimeZone.getDefault().getRawOffset())/ 1000.0) / 3600;
        // Test Prayer times here
        prayers = new PrayTimeCalcul();

        prayers.setTimeFormat(prayers.getTime12());

        prayers.setAdjustHighLats(prayers.getAngleBased());
        int[] offsets = {0, 0, 0, 0, 0, 0, 0}; // {Fajr,Sunrise,Dhuhr,Asr,Sunset,Maghrib,Isha}
        prayers.tune(offsets);

        Date now = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(now);

        ArrayList<String> prayerTimes = prayers.getPrayerTimes(cal,latitude, longitude, timezone);

        fajer.setText(fajer.getText()+" : "+prayerTimes.get(0));
        zuhur.setText(zuhur.getText() + " : "+prayerTimes.get(2));
        asar.setText(asar.getText()+" : "+prayerTimes.get(3));
        maghrib.setText(maghrib.getText()+" : "+prayerTimes.get(5));
        icha.setText(icha.getText()+" : "+prayerTimes.get(6));

    }
}
